

/**
  Controller object that operates on class hooks within a HTML template with a unique id.
  Multiple templates can be used  with an instance of this class for each.
  Currency conversion  defaults to GBP-AUD though any suppoerted combination can be passed in.
*/
class CurrencyConverter {

  /**
    Constructor:
    Instantiates the id specific template nodes and binds the required event listeners.
  */
  constructor(id, sourceCurrency="GBP", targetCurrency="AUD") {
    this.sourceCurrency = sourceCurrency;
    this.targetCurrency =  targetCurrency;
    this.sourceCurrencyField = document.querySelector(`#${id} .sourceCurrencyField`);
    this.targetCurrencyField = document.querySelector(`#${id} .targetCurrencyField`);
    this.timeWrapper    = document.querySelector(`#${id} .time`);
    this.messageWrapper = document.querySelector(`#${id} .timeMessage`);
    this.conversionRateMessage = document.querySelector(`#${id} .conversionRateMessage`);
    this.conversionAmountMessage = document.querySelector(`#${id} .conversionAmountMessage`);
    this.errorWrapper   = document.querySelector(`#${id} .error`);
    this.convertButton  = document.querySelector(`#${id} .convertButton`);

    this.sourceCurrencyField.addEventListener('click', () =>{
      this.sourceCurrencyField.value ="";
      this.resetUI();
    });

    this.sourceCurrencyField.addEventListener('keypress', (e) =>{
      if(e.keyCode === 13){
        e.preventDefault();
        this.convert();
      }
    });

    this.convertButton.addEventListener('click', () =>{
     this.convert();
    });
    this.expiryTimer = undefined;

    this.resetUI();
  }

  /**
    Called when the convert button is clicked.
    Validated then conditionally performs API request for conversion rate
  */
  convert(){
    if(this.expiryTimer){
      clearInterval(this.expiryTimer);
      this.expiryTimer = undefined;
    }
    this.resetUI();

    if(this.validate()){
      fetch(`https://api.exchangerate-api.com/v4/latest/${this.sourceCurrency}`)
      .then(response => response.json())
      .then(data => {
        if(data && data.rates && data.rates[this.targetCurrency]){
          const rate = data.rates[this.targetCurrency];
          let targetValue = rate * this.sourceCurrencyField.value;
          this.renderSuccess(rate, targetValue);
        }else{
          throw 'Unexpected response';
        }
      })
      .catch((error) => {
        this.renderError("Sorry.. please try again");
        console.error(error);
      });
    }
  }

  /**
    Validates the source currency value and determines error state or to continue.
  */
  validate(){
    const sourceValue = this.sourceCurrencyField.value;
    if(!sourceValue || sourceValue.length < 0){
      this.renderError(`${this.sourceCurrency} amount required`);
      return false;
    }else if(isNaN(sourceValue)){
      this.renderError(`Numeric value required for ${this.sourceCurrency} amount`);
      return false;
    }
    else if(parseFloat(sourceValue) <= 0){
      this.renderError(`Value greater than zero required for ${this.sourceCurrency} amount`);
      return false;
    }
    return true;
  }

  /**
    Displays the ui state for an error
  */
  renderError(message){
    this.sourceCurrencyField.classList.add("error");
    this.errorWrapper.style.display="block";
    this.errorWrapper.innerHTML=message;
  }

  /**
    Renders to ui state for a successful conversion event.
  */
  renderSuccess(rate, targetValue){
    if(this.targetCurrencyField){
      this.targetCurrencyField.value = parseFloat(targetValue).toFixed(2);
    }

    if(this.conversionRateMessage){
       this.conversionRateMessage.style.display="block";
       this.conversionRateMessage.innerHTML =`1 ${this.sourceCurrency} is equivalent to ${rate.toFixed(2)} ${this.targetCurrency}`;
    }

    if(this.conversionAmountMessage){
       this.conversionAmountMessage.style.display="block";
       this.conversionAmountMessage.innerHTML =`1 ${this.sourceCurrency} is equivalent to ${rate.toFixed(2)} ${this.targetCurrency}`;
    }

    if(this.timeWrapper){
      let expirySeconds = 600;
      this.messageWrapper.style.display="block";
      let time = new Date(expirySeconds * 1000).toISOString().substr(14, 5);
      this.timeWrapper.innerHTML =`${time}`;

      this.expiryTimer = setInterval( () =>{  
          expirySeconds = expirySeconds - 1;
          if(expirySeconds < 0){
                  clearInterval(this.expiryTimer);
                  this.resetUI();
          }else{
            time = new Date(expirySeconds * 1000).toISOString().substr(14, 5);
            this.timeWrapper.innerHTML =`${time}`;
          }
        },1000);
     }
  }

  /**
   Resets the UI to initial state, though preserving the source value
  */
  resetUI(){
    this.errorWrapper.style.display="none";
    this.messageWrapper.style.display="none";
    if(this.targetCurrencyField){
      this.targetCurrencyField.value ="";
    }

    if(this.conversionRateMessage){
       this.conversionRateMessage.innerHTML ="Rates of exchange are valid for 10 minutes.";
    }

    if(this.conversionAmountMessage){
       this.conversionAmountMessage.style.display="none";
       this.conversionAmountMessage.innerHTML ="";
    }
    this.sourceCurrencyField.classList.remove("error");
  }
}


// Template code for 1st example that is a progression from the wireframe in the spec.
const convertor1Id = "converter-gbp-aud"
let component1 = document.createElement("div");
component1.id=convertor1Id;
component1.classList.add("currency-form");
component1.innerHTML = "\
 <form >\
   <legend>British pounds to Australian dollars</legend>\
   <fieldset>\
     <input class='sourceCurrencyField '  type='text' name='sourceCurrencyField'  aria-required='true' required  aria-label='GBP value'  placeholder='Amount (GBP)' tabindex='0' maxLength='12' />\
     <img src='./images/flag-round-250-GBP.png' title='British pounds' alt='British pounds' width='40' height='40'>\
   </fieldset>\
   <fieldset>\
     <input class='targetCurrencyField' type='text' aria-label='AUD value' name='sourceCurrencyField' placeholder='Amount (AUD)' readonly tabindex='-1'  />\
     <img src='./images/flag-round-250-AUD.png' title='Australian dollars' alt='Australian dollars' width='40' height='40'>\
     <p class='conversionRateMessage' ></p>\
   </fieldset>\
   <div class='info' >\
     <p class='timeMessage' style='display:none;'>Expires in <span class='time'></span></p>\
     <p class='error'   style='display:none;'></p>\
   </div>\
   <input class='convertButton' type='button' value='Convert' name='convertButton' tabindex='0' aria-label='Convert GBP to AUD' />\
 </form>";
document.getElementById("root").appendChild(component1);


// Template code for a 2nd example to demstrate multiple instances and component reuse.
const convertor2Id = "converter-gbp-eur"
let component2 = document.createElement("div");
component2.id=convertor2Id;
component2.classList.add("currency-form");
component2.innerHTML = "\
 <form >\
   <legend>British pounds to EU Euros</legend>\
   <fieldset>\
     <input class='sourceCurrencyField '  type='text' name='sourceCurrencyField'  aria-required='true' required  aria-label='GBP value'  placeholder='Amount (GBP)' tabindex='0' maxLength='12' />\
     <img src='./images/flag-round-250-GBP.png' title='British pounds' alt='British pounds' width='40' height='40'>\
   </fieldset>\
   <fieldset>\
     <input class='targetCurrencyField' type='text' aria-label='EUR value' name='sourceCurrencyField' placeholder='Amount (EUR)' readonly tabindex='-1'  />\
     <img src='./images/flag-round-250-EUR.png' title='EU Euros' alt='EU Euros' width='40' height='40'>\
     <p class='conversionRateMessage' ></p>\
   </fieldset>\
   <div class='info' >\
     <p class='timeMessage' style='display:none;'>Expires in <span class='time'></span></p>\
     <p class='error'   style='display:none;'></p>\
   </div>\
   <input class='convertButton' type='button' value='Convert' name='convertButton' tabindex='0' aria-label='Convert GBP to EUR' />\
 </form>";
document.getElementById("root").appendChild(component2);


// Instatiation of controller instances, with twmplate ids passed in + optional currency parameters.
const converter1 = new CurrencyConverter(convertor1Id);
const converter2 = new CurrencyConverter(convertor2Id,"GBP","EUR");
